0.0.3 (2024-06-26)
------------------

-   fix README in pyproject.toml


0.0.2 (2024-06-26)
------------------

-   update castellum link in README
-   switch to pyproject.toml


0.0.1 (2020-09-30)
------------------

initial release
