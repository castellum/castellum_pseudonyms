import unittest

from castellum_pseudonyms import clean, generate


class TestPseudonyms(unittest.TestCase):
    def test_clean_generated(self):
        clean(generate())

    def test_clean_invalid_checksum(self):
        with self.assertRaises(ValueError) as cm:
            clean('foo')
        self.assertNotIn('invalid characters', str(cm.exception))

    def test_clean_invalid_chars(self):
        with self.assertRaises(ValueError) as cm:
            clean('ABC!')
        self.assertIn('invalid characters', str(cm.exception))

    def test_generate_len(self):
        self.assertEqual(len(generate()), 10)

    def test_check_denormalized(self):
        self.assertEqual(clean('vg83END03T'), 'VG83END03T')
        self.assertEqual(clean('VG83ENDO3T'), 'VG83END03T')


if __name__ == '__main__':
    unittest.main()
